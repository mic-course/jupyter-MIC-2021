# 2021 MIC Jupyter Container Image
This image was used in the 2021 MIC Course

This is the source for the Docker container used to run the course Jupyter
notebooks. 



# Using the image
## Install docker

To run a container on your local machine or laptop, download the docker program from <https://www.docker.com>. 
## Run image on your local computer

Once you have the docker program installed, open the program (you should get a terminal screen with command line). Enter the command:
```
docker pull miccourse/jupyter-mic-2021:2021_final
```

This will pull down the course docker image from dockerhub. It may take a few minutes. Next, run the command to start a container:
```
docker run \
    --rm -p 8888:8888 \
    -v $HOME:/home/jovyan \
    -t miccourse/jupyter-mic-2021:2021_final \
    /bin/bash -c "/usr/local/bin/start.sh jupyter notebook --ip=0.0.0.0 --notebook-dir=/home/jovyan --no-browser"
```
When you run this command, Jupyter will print something like the following. Copy the last URL (the one that starts with "http://127.0.0.1") from what Jupyter prints and open it in your web browser.
```
    To access the notebook, open this file in a browser:
        file:///home/jovyan/.local/share/jupyter/runtime/nbserver-8-open.html
    Or copy and paste one of these URLs:
        http://36eddc7fbb33:8888/?token=9988732150d6adf46dec30abf52722d27bcb94952bafba9b
     or http://127.0.0.1:8888/?token=9988732150d6adf46dec30abf52722d27bcb94952bafba9b
```
The home directory *on your host computer* (e.g. your laptop) available as /home/jovyan within the Jupyter container and all the *software* in the image will be available within Jupyter.

> There is some security risk of running with this command, because your Jupyter session may be accessible from outside your computer

### Stopping Docker
The container will continue running, even if you do not have Jupyter open in a web browser.  If you don't plan to use it for a while, you might want to shut it down so it isn't using resources on your computer.  Here are two ways to do that:

1. Click on "Quit" in the Jupyter file broswer.
1. Type control-C twice In the terminal where you ran docker.

#### Commandline
You may want to familiarize yourself with the following Docker commands.
-   `docker stop`
-   `docker rm`
-   `docker ps -a`
-   `docker images`
-   `docker rmi`

### Windows Note
These instructions have not been tested in a Windows environment.  If you have problems with them, please give us feedback

## Running the Course Image with Singularity
Docker requires root permissions to run, so you are unlikely to be able to run Docker on a computer that you are not fully in control of.  As an alternative you can run the course image with [Singularity](https://sylabs.io/singularity/), another container system. Singularity is similar to Docker, and can run Docker images, but you do not need special permissions to run Singularity images *or* Docker images with Singularity (as long as Singularity is actually installed on the computer).

The following command uses Singularity to start up a container from the course Jupyter image.
```
singularity exec docker://miccourse/jupyter-mic-2021:2021_final /usr/local/bin/start.sh jupyter notebook --ip=0.0.0.0 --no-browser
```
> Note: the first time you run this, it might take a VERY long time to download the Docker image and build the Singularity image from it


### Running the Course Image on a SLURM cluster

We will use the example of the Duke Computer Cluster, but these instructions should be easily adaptable to other clusters

1. From your computer run this to connect to DCC:
```
ssh NetID@dcc-login-03.oit.duke.edu
```
2. Once you are connected run this to start a tmux session:
```
tmux new -s jupyter
```
3. Once you have started a tmux session you can start up Jupyter with this command:
```
srun singularity exec docker://miccourse/jupyter-mic-2021:2021_final /usr/local/bin/start.sh jupyter notebook --ip=0.0.0.0 --no-browser
```
> Note: the first time you run this, it might take a VERY long time to download the Docker image and build the Singularity image from it

Running this command will print a bunch of stuff. You can ignore everything except the last two lines, which will say something like:
```
http://dcc-chsi-01:8889/?token=08172007896ad29bb5fbd92f6f3f516a8b2f7303ed7f1df3
or http://127.0.0.1:8889/?token=08172007896ad29bb5fbd92f6f3f516a8b2f7303ed7f1df3
```

You need this information for the next few steps. For the next step you need the “dcc-chsi-01:8889” part.
“dcc-chsi-01” is the compute node that Jupyter is running on and “8889” is the port it is listening on. You may get a different value every time you start the container.

4. You want to run the following command in another terminal on your computer to set up port forwarding.
```
ssh -L PORT:NODE.rc.duke.edu:PORT NetID@dcc-login-03.oit.duke.edu
```
In this command you want to replace “PORT” with the value you got for port from the srun command and replace “NODE” with the compute node that was printed by the srun command. So for the example above, the ssh port forwarding command would be:

```
ssh -L 8889:dcc-chsi-01.rc.duke.edu:8889 NetID@dcc-login-03.oit.duke.edu
```

5. Now you can put the last line that the srun command printed in your web browser and it should open your Jupyter instance running on DCC.

#### Notes
1. The Jupyter session keeps running until you explicitly shut it down.  If the port forwarding SSH connection drops you will need to restart SSH with the same command, but you don’t need to restart Jupyter.

2. There are two ways to explicitly shut down Jupyter:
    1. Within Jupyter, click on the *Jupyter* logo in the top left to go to the main Jupyter page, then click "Quit" in the top right
    2. Do control-C twice in the terminal where you started Jupyter. If this connection dropped, you can reconnect to it with:
    ```
    ssh NetID@dcc-login-03.oit.duke.edu
    tmux a -t jupyter
    ```
    After shutting down the Jupyter session you can type `exit` at the terminal to close the tmux session.

3. If you need more memory or more cpus you can use the `--mem` and/or `--cpus-per-task` arguments to in the “srun”, for example to request 4 CPUs and 10GB of RAM:
```
srun --cpus-per-task=4 --mem=10G singularity exec docker://miccourse/jupyter-mic-2021:2021_final /usr/local/bin/start.sh jupyter notebook --ip=0.0.0.0 --no-browser
```

4. If you have high priority access to a partition you can request that partition be used with the `-A` and `-p` arguments to `srun`:
```
srun -A chsi -p chsi srun singularity exec docker://miccourse/jupyter-mic-2021:2021_final /usr/local/bin/start.sh jupyter notebook --ip=0.0.0.0 --no-browser
```

5. You might want to access files that are outside of your home directory. Within a singularity container your access to the host computer is
    limited: by default, from inside the container you can only access your home directory. If you want to access directories that are outside your home
    directory, you have to tell *Singularity* when you start the container with the `--bind` command line argument. For example:

```
srun singularity exec --bind /work/josh:/work/josh  docker://miccourse/jupyter-mic-2021:2021_final /usr/local/bin/start.sh jupyter notebook --ip=0.0.0.0 --no-browser
```

6. You can combine several of these command line flags:
```
srun -A chsi -p chsi --cpus-per-task=4 --mem=10G singularity  exec --bind /work/josh:/work/josh docker://miccourse/jupyter-mic-2021:2021_final /usr/local/bin/start.sh jupyter notebook --ip=0.0.0.0 --no-browser
```

6. It is strongly recommended to set the `SINGULARITY_CACHEDIR` environment variables in your .bashrc or when running `srun`. This environment variable specifies where the Docker image (and the Singularity image built from it) are saved. If this variable is not specified, singularity will cache images in `$HOME/.singularity/cache`, which can fill up quickly. This is discussed in the [Singularity Documentation](https://sylabs.io/guides/3.7/user-guide/build_env.html#cache-folders)

```
export SINGULARITY_CACHEDIR="/work/josh/singularity_cache"; srun -A chsi -p chsi --cpus-per-task=4 --mem=10G singularity  exec --bind /work/josh:/work/josh docker://miccourse/jupyter-mic-2021:2021_final /usr/local/bin/start.sh jupyter notebook --ip=0.0.0.0 --no-browser
```

### Install Singularity
Here are instructions for installing:

- [Singularity version 2.6](https://sylabs.io/guides/2.6/user-guide/quick_start.html#quick-installation-steps)
- [Singularity version 3.2](https://sylabs.io/guides/3.2/user-guide/quick_start.html#quick-installation-steps)
- [Singularity Desktop for macOS (Alpha Preview)](https://sylabs.io/singularity-desktop-macos/)
