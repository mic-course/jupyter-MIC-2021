# Singularity Container Example


* The Singularity images created are stored on research-singularity-registry.oit.duke.edu. You can pull your image down to the DCC by logging into one of the DCC login servers and pulling your image down by _curl_: 

```
curl -k -O https://research-singularity-registry.oit.duke.edu/mic-course/jupyter-MIC-2021.sif
```

* To run the image:
``` 
srun -A chsi -p chsi --cpus-per-task=4 --mem=10G singularity exec jupyter-MIC-2021.sif /usr/local/bin/start.sh jupyter notebook --ip=0.0.0.0 --no-browser
```
